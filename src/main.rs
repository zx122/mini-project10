use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};

use std::convert::Infallible;
use std::io::Write;
// use log::{info, trace, warn};
use std::path::PathBuf;


/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples


fn generate_response(prompt: String) -> Result<String, Box<dyn std::error::Error>> {
    // Set the model's details and the tokenizer
    let tokenizer = llm::TokenizerSource::Embedded;
    let architecture = llm::ModelArchitecture::GptNeoX;
    
    // Define model's path based on environment
    let model_file = PathBuf::from("./src/pythia-70m-q4_0-ggjt.bin");

    let model = llm::load_dynamic(
        Some(architecture),
        &model_file,
        tokenizer,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;

    let mut session = model.start_session(Default::default());
    let mut response_text = String::new();

    let inference_result = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &llm::InferenceRequest {
            prompt: (&prompt).into(),
            parameters: &llm::InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(15),
        },
        &mut Default::default(),
        |response| match response {
            llm::InferenceResponse::PromptToken(token) | llm::InferenceResponse::InferredToken(token) => {
                print!("{token}");
                std::io::stdout().flush().unwrap();
                response_text.push_str(&token);
                Ok(llm::InferenceFeedback::Continue)
            }
            _ => Ok(llm::InferenceFeedback::Continue),
        },
    );

    // Handle inference result
    match inference_result {
        Ok(_) => Ok(response_text),
        Err(e) => Err(Box::new(e)),
    }
}


async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract some useful information from the request
    let query_info = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("text"))
        .unwrap_or("Emily");

    // Generate response based on user query
    let message = match generate_response(query_info.to_string()) {
        Ok(result) => result,
        Err(e) => format!("Error Info: {:?}", e),
    };
    // log::info!("Print the response from the model: {:?}", message);
    println!("Response from model: {:?}", message);

    // Return something that implements IntoResponse.
    // It will be serialized to the right response event automatically by the runtime
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(message.into())
        .map_err(Box::new)?;
    Ok(resp)
}


#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
