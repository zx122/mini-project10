# Use the cargo-lambda image for building
FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

# Set working directory
WORKDIR /usr/src/myapp

# Copy Cargo.toml and Cargo.lock
COPY Cargo.toml Cargo.lock ./

# Build dependencies to cache them
RUN mkdir src \
    && echo "fn main() {println!(\"Dummy main function\")}" > src/main.rs \
    && cargo build --release \
    && rm -rf src Cargo.toml Cargo.lock

# Copy the actual source code
COPY src ./src/

# Build the application
RUN cargo build --release

# Final stage
FROM public.ecr.aws/lambda/provided:al2-arm64

# Set working directory
WORKDIR /var/task

# Copy the binary from the builder stage
COPY --from=builder /usr/src/myapp/target/release/myapp ./bootstrap

# Set the CMD to the binary
CMD ["./bootstrap"]